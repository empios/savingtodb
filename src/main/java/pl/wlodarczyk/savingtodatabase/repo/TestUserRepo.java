package pl.wlodarczyk.savingtodatabase.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.wlodarczyk.savingtodatabase.model.TestUser;


@Repository
public interface TestUserRepo extends JpaRepository<TestUser, Long> {
}
