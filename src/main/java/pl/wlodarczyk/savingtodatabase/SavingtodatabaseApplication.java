package pl.wlodarczyk.savingtodatabase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SavingtodatabaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(SavingtodatabaseApplication.class, args);
    }

}
