package pl.wlodarczyk.savingtodatabase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.wlodarczyk.savingtodatabase.model.TestUser;
import pl.wlodarczyk.savingtodatabase.repo.TestUserRepo;


@RestController
public class Controller {

    private TestUserRepo testUserRepo;
    public BCryptPasswordEncoder passwordEncoder;

    @Bean
    PasswordEncoder getPassword(){
        return new BCryptPasswordEncoder();
    }


    @Autowired
    public Controller(TestUserRepo testUserRepo) {
        this.testUserRepo = testUserRepo;
        this.passwordEncoder = new BCryptPasswordEncoder();
    }

    @GetMapping("/save")
    public String save(){
        TestUser testUser = new TestUser("test", passwordEncoder.encode("test123"));
        testUserRepo.save(testUser);
        return "Zapisano";
    }

    @GetMapping("/show")
    public String show(){
        TestUser testUser = testUserRepo.findAll().get(0);
        return testUser.getUsername() + " " + testUser.getPassword();
    }

}
